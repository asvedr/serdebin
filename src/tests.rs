use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::{from_bytes, to_bytes};

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
enum MyEnum {
    VarA,
    VarB(String),
    VarC { a: i8, b: char },
}

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
struct Point {
    x: isize,
    y: isize,
}

#[test]
fn test_vec_string() {
    let original = vec!["abc".to_string(), "def".to_string()];
    let bts = to_bytes(&original).unwrap();
    let (restored, rest) = from_bytes::<Vec<String>>(&bts).unwrap();
    assert_eq!(original, restored);
    assert!(rest.is_empty());
}

#[test]
fn test_map() {
    let original: HashMap<char, i64> = [('a', 1), ('ё', 2), (' ', -10)].into_iter().collect();
    let bts = to_bytes(&original).unwrap();
    let (restored, rest) = from_bytes::<HashMap<char, i64>>(&bts).unwrap();
    assert_eq!(original, restored);
    assert!(rest.is_empty());
}

#[test]
fn test_enum() {
    let original = vec![
        MyEnum::VarA,
        MyEnum::VarB("111".to_string()),
        MyEnum::VarC { a: -1, b: '😀' },
    ];
    let bts = to_bytes(&original).unwrap();
    println!(">> {:?}", bts);
    let (restored, rest) = from_bytes::<Vec<MyEnum>>(&bts).unwrap();
    assert_eq!(original, restored);
    assert!(rest.is_empty());
}

#[test]
fn test_struct() {
    let original = Point { x: 11, y: 12 };
    let bts = to_bytes(&original).unwrap();
    let (restored, rest) = from_bytes::<Point>(&bts).unwrap();
    assert_eq!(original, restored);
    assert!(rest.is_empty());
}

#[test]
fn test_longer_nums() {
    let original: Vec<String> = (0..1000)
        .map(|i| (0..i).map(|_| 'x').collect::<String>())
        .collect();
    let bts = to_bytes(&original).unwrap();
    let (restored, rest) = from_bytes::<Vec<String>>(&bts).unwrap();
    assert_eq!(original, restored);
    assert!(rest.is_empty());
}
