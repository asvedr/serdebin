pub(crate) const U8_MAX: usize = u8::MAX as usize;
pub(crate) const U16_MAX: usize = u16::MAX as usize;
pub(crate) const U32_MAX: usize = u32::MAX as usize;

pub(crate) const U8_TAG: u8 = 0_u8.to_le();
pub(crate) const U16_TAG: u8 = 1_u8.to_le();
pub(crate) const U32_TAG: u8 = 2_u8.to_le();
pub(crate) const U64_TAG: u8 = 3_u8.to_le();
