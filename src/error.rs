use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub enum SerializerError {
    UnexpectedEOF,
    InvalidData,
    Custom(String),
}

impl Display for SerializerError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for SerializerError {}

impl serde::ser::Error for SerializerError {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Self::Custom(msg.to_string())
    }
}

impl serde::de::Error for SerializerError {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Self::Custom(msg.to_string())
    }
}
