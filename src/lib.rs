mod consts;
mod de;
mod error;
mod ser;
#[cfg(test)]
mod tests;

use serde::{Deserialize, Serialize};

pub use crate::error::SerializerError;

pub fn to_bytes<T: Serialize>(val: T) -> Result<Vec<u8>, SerializerError> {
    let mut serializer = ser::BinSerializer::default();
    val.serialize(&mut serializer)?;
    Ok(serializer.bts)
}

pub fn from_bytes<'a, T: Deserialize<'a>>(src: &'a [u8]) -> Result<(T, &'a [u8]), SerializerError> {
    let mut deserializer = de::BinDeserializer { bts: src };
    let val = T::deserialize(&mut deserializer)?;
    // Ok(val)
    Ok((val, deserializer.bts))
}
